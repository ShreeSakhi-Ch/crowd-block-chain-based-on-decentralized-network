package com;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.File;
import java.util.ArrayList;
public class Reward extends HttpServlet {
public void doGet(HttpServletRequest request, HttpServletResponse response)	throws ServletException, IOException {
	response.setContentType("text/html");
	PrintWriter out = response.getWriter();
	String worker=request.getParameter("t11").trim();
	String file=request.getParameter("t2").trim();
	String reward=request.getParameter("t3").trim();
	try{
		String path = getServletContext().getRealPath("/")+"WEB-INF/task.txt";
		File file_path = new File(path);
		ObjectInputStream oin = new ObjectInputStream(new FileInputStream(file_path));
		Object obj = (Object)oin.readObject();
		ArrayList<Task> tasklist = (ArrayList<Task>)obj;
		for(int i=0;i<tasklist.size();i++){
			Task taskdata = tasklist.get(i);
			if(taskdata.getWorker().equals(worker) && taskdata.getTaskFile().equals(file)){
				taskdata.setReward(reward);
				break;
			}
		}
		oin.close();
		ObjectOutputStream oout = new ObjectOutputStream(new FileOutputStream(file_path));
		oout.writeObject(tasklist);
		oout.flush();
		oout.close();
		RequestDispatcher rd=request.getRequestDispatcher("RequesterScreen.jsp?t1=Reward updated successfully");
		rd.forward(request, response);
		
		
	}catch(Exception e){
		e.printStackTrace();
	}
}

}
