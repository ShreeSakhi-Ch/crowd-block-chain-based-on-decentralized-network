package com;
import java.io.Serializable;
public class Task implements Serializable{
	String task_file,solution_file;
	String requester;
	String worker;
	byte task[];
	byte solution[];
	String reward;

public void setReward(String reward) {
	this.reward = reward;
}
public String getReward(){
	return reward;
}
public void setTaskFile(String task_file){
	this.task_file = task_file;
}
public String getTaskFile(){
	return task_file;
}

public void setSolutionFile(String solution_file){
	this.solution_file = solution_file;
}
public String getSolutionFile(){
	return solution_file;
}

public void setRequester(String requester){
	this.requester = requester;
}
public String getRequester(){
	return requester;
}

public void setWorker(String worker){
	this.worker = worker;
}
public String getWorker(){
	return worker;
}

public void setTask(byte task[]){
	this.task = task;
}
public byte[] getTask(){
	return task;
}

public void setSolution(byte solution[]){
	this.solution = solution;
}
public byte[] getSolution(){
	return solution;
}
}