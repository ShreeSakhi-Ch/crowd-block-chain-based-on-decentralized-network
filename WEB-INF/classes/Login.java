package com;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;

public class Login extends HttpServlet {
public void doPost(HttpServletRequest request, HttpServletResponse response)	throws ServletException, IOException {
	response.setContentType("text/html");
	PrintWriter out = response.getWriter();
	String user=request.getParameter("t1").trim();
	String pass=request.getParameter("t2").trim();
	try{
		String path = getServletContext().getRealPath("/")+"WEB-INF/classes/address.txt";
		Path.setPath(path);
		long start = System.currentTimeMillis();
		ArrayList<String> record = AccessSmartContract.getUser();
		long end = System.currentTimeMillis();
		TimeComputation.setTime((end-start));
		TimeComputation.setTask(1);
		String msg = "none";
		for(int i=0;i<record.size();i++){
			String arr[] = record.get(i).split(",");
			if(arr[0].trim().equals(user) && arr[1].trim().equals(pass)) {
				msg = arr[5].trim();
				break;
			}
		}
		if(msg.equals("Worker")){
			HttpSession session=request.getSession();
			session.setAttribute("user",user);
			response.sendRedirect("WorkerScreen.jsp?t1=Welcome "+user);
		}
		else if(msg.equals("Requester")){
			HttpSession session=request.getSession();
			session.setAttribute("user",user);
			response.sendRedirect("RequesterScreen.jsp?t1=Welcome "+user);
		}
		else{
			RequestDispatcher rd=request.getRequestDispatcher("Login.jsp?t1=invalid username or password");
			rd.forward(request, response);
		}
		
	}catch(Exception e){
		e.printStackTrace();
	}
}

}
