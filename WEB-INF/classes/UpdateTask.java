package com;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
public class UpdateTask extends HttpServlet {
public void doPost(HttpServletRequest request, HttpServletResponse response)	throws ServletException, IOException {
	response.setContentType("text/html");
	byte b[]=null;
	String worker = null;
	String task = null;
	String status = null;	
	String file = null;
	PrintWriter out = response.getWriter();
	DiskFileItemFactory factory = new DiskFileItemFactory();
	factory.setSizeThreshold(10*1024*1024);
	factory.setRepository(new File("C:/usr"));
    ServletFileUpload upload = new ServletFileUpload(factory);
	upload.setSizeMax(10*1024*1024);
	try{
		List fileItems = upload.parseRequest(request);
		Iterator itr = fileItems.iterator();
		while(itr.hasNext()) {
			FileItem item = (FileItem) itr.next();
			if(item.isFormField()){
				String name = item.getFieldName();
		        String value = item.getString();
				if(name.equals("t1")){
					worker = value;
				}
				if(name.equals("t2")){
					task = value;
				}
				if(name.equals("t3")){
					status = value;
				}
			}else{
				file = item.getName();
				InputStream in=item.getInputStream();
				b = new byte[in.available()];
				in.read(b,0,b.length);
			}
		}
		HttpSession session = request.getSession();
		String user = (String)session.getAttribute("user");
		String path = getServletContext().getRealPath("/")+"WEB-INF/task.txt";
		File file_path = new File(path);
		ObjectInputStream oin = new ObjectInputStream(new FileInputStream(file_path));
		Object obj = (Object)oin.readObject();
		ArrayList<Task> tasklist = (ArrayList<Task>)obj;
		for(int i=0;i<tasklist.size();i++){
			Task taskdata = tasklist.get(i);
			if(taskdata.getTaskFile().equals(task)){
				taskdata.setSolutionFile(file);
				taskdata.setSolution(b);
				taskdata.setWorker(worker);
				break;
			}
		}
		oin.close();
		ObjectOutputStream oout = new ObjectOutputStream(new FileOutputStream(file_path));
		oout.writeObject(tasklist);
		oout.flush();
		oout.close();
		RequestDispatcher rd=request.getRequestDispatcher("WorkerScreen.jsp?t1=task status updated successfully");
		rd.forward(request, response);
	}catch(Exception e){
		e.printStackTrace();
	}
}

}
