<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>Crowdsourcing</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="style.css" />
<script language="javascript">
	function validate(formObj)
	{
	if(formObj.t1.value.length==0)
	{
	alert("Please Enter task name");
	formObj.t1.focus();
	return false;
	}
	if(formObj.t2.value.length==0)
	{
	alert("Please upload task details");
	formObj.t2.focus();
	return false;
	}
	formObj.actionUpdateData.value="update";
	return true;
	}
	</script>
</head>
<body>
<div id="wrapper">
	<div id="header">
		<div id="logo">
			<h1>CrowdBC: A Blockchain-based Decentralized
Framework for Crowdsourcing</h1>
		</div>
		<div id="slogan">
			
		</div>
	</div>
	<div id="menu">
		<ul>
			<li class="first current_page_item"><a href="Publish.jsp">Publish Task</a></li>
			<li><a href="Judgement.jsp">Reward/Penalty Rule</a></li>
			<li><a href="FinalSolution.jsp">Receive Final Solution</a></li>
			<li><a href="Chart.jsp">Computation Chart</a></li>
			<li><a href="Logout.jsp">Logout</a></li>
		</ul>					<br class="clearfix" />
				</div>
			
	<div id="splash">
		<img class="pic" src="images/investor.jpg" width="870" height="230" alt="" />
	</div>	
			<center>
<form name="f1" method="post" action="Publish" enctype="multipart/form-data" onsubmit="return validate(this);"><br/>
   <h2><b>Requester Task Publish Screen</b></h2>
   
	<%
	String res = request.getParameter("t1");
	if(res != null){
		out.println("<center><font face=verdana color=red>"+res+"</center></font>");
	}%>
						
						<table align="center" width="40" >
			 <tr><td><b>Task&nbsp;Name</b></td><td><input type="text" name="t1" style="font-family: Comic Sans MS" size=30/></td></tr>
         
		  <tr><td><b>Upload&nbsp;Task&nbsp;Details</b></td><td><input type="file" name="t2" style="font-family: Comic Sans MS" size=30/></td></tr>

		  <tr><td><b>Time&nbsp;Limit&nbsp;Days</b></td><td><select name="t3">
		  <%for(int i=1;i<100;i++){%>
		  <option value="<%=i%>"><%=i%></option>
			  <%}%>
		  </select>
		  </td></tr>
         
			<tr><td></td><td><input type="submit" value="Publish"></td>
			</table>
				</div>	
					
				</div>
				
	</body>
</html>