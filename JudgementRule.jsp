<%@page import="java.util.ArrayList"%>
<%@page import="java.io.ObjectOutputStream"%>
<%@page import="java.io.ObjectInputStream"%>
<%@page import="java.io.File"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>Crowdsourcing</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
<div id="wrapper">
	<div id="header">
		<div id="logo">
			<h1>>CrowdBC: A Blockchain-based Decentralized
Framework for Crowdsourcing</h1>
		</div>
		<div id="slogan">
			
		</div>
	</div>
	<div id="menu">
		<ul>
			<li class="first current_page_item"><a href="Publish.jsp">Publish Task</a></li>
			<li><a href="Judgement.jsp">Reward/Penalty Rule</a></li>
			<li><a href="FinalSolution.jsp">Receive Final Solution</a></li>
			<li><a href="Chart.jsp">Computation Chart</a></li>
			<li><a href="Logout.jsp">Logout</a></li>
		</ul>				<br class="clearfix" />
				</div>
			
	<div id="splash">
		<img class="pic" src="images/investor.jpg" width="870" height="230" alt="" />
	</div>	
			<center>
<br/>
   <h2><b>View Publish Task Screen</b></h2>
   
	<%
	String res = request.getParameter("t1");
	if(res != null){
		out.println("<center><font face=verdana color=red>"+res+"</center></font>");
	}%>
						
						<table align="center" border="1">
	<tr><th>Worker Name</th>
	<th>Solution File</th><th>Download Solution</th><th>Reward</th>
	<th>Penalty</th>
		 <%
	String user = session.getAttribute("user").toString();
	String file = request.getParameter("t11");
	String path = getServletContext().getRealPath("/")+"WEB-INF/task.txt";
	File file_path = new File(path);
	ObjectInputStream oin = new ObjectInputStream(new java.io.FileInputStream(file_path));
	Object obj = (Object)oin.readObject();
	oin.close();
	ArrayList<com.Task> tasklist = (ArrayList<com.Task>)obj;
	for(int i=0;i<tasklist.size();i++){
		com.Task task = tasklist.get(i);
		if(task.getRequester().equals(user) && task.getTaskFile().equals(file)){%>
	
	
<tr><td><font size="" color="black"><%=task.getWorker()%></td>
<td><font size="" color="black"><%=task.getSolutionFile()%></td>
<td><a href="DownloadSolution.jsp?t11=<%=task.getSolutionFile()%>"><font size="" color="black">Click Here</a></td>
<td><a href="Reward?t11=<%=task.getWorker()%>&t2=<%=file%>&t3=Right"><font size="" color="black">Click Here</a></td>
<td><a href="Reward?t11=<%=task.getWorker()%>&t2=<%=file%>&t3=Wrong"><font size="" color="black">Click Here</a></td>
	<%}}%>
	</tr>
    		</table>
				</div>	
					
				</div>
				<br/><br/>
	</body>
</html>