package com;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;

public class Publish extends HttpServlet {
public void doPost(HttpServletRequest request, HttpServletResponse response)	throws ServletException, IOException {
	response.setContentType("text/html");
	byte b[]=null;
	String file = null;
	String tasks = null;
	String time = null;	
	PrintWriter out = response.getWriter();
	DiskFileItemFactory factory = new DiskFileItemFactory();
	factory.setSizeThreshold(10*1024*1024);
	factory.setRepository(new File("C:/usr"));
    ServletFileUpload upload = new ServletFileUpload(factory);
	upload.setSizeMax(10*1024*1024);
	try{
		List fileItems = upload.parseRequest(request);
		Iterator itr = fileItems.iterator();
		while(itr.hasNext()) {
			FileItem item = (FileItem) itr.next();
			if(item.isFormField()){
				String name = item.getFieldName();
		        String value = item.getString();
				if(name.equals("t1")){
					tasks = value;
				}
				if(name.equals("t3")){
					time = value;
				}
			}else{
				file = item.getName();
				InputStream in=item.getInputStream();
				b = new byte[in.available()];
				in.read(b,0,b.length);
			}
		}
		String path = getServletContext().getRealPath("/")+"WEB-INF/task.txt";
		File file_path = new File(path);
		HttpSession session = request.getSession();
		String user = (String)session.getAttribute("user");
		java.util.Date dd = new java.util.Date();
		java.sql.Timestamp datetime = new java.sql.Timestamp(dd.getTime());
		String output = user+","+tasks+","+datetime+","+time+","+file;
		long start = System.currentTimeMillis();
		String msg = AccessSmartContract.publishTask(output);
		long end = System.currentTimeMillis();
		TimeComputation.setTime((end-start));
		TimeComputation.setTask(1);
		if(msg.equals("success")){
			Task task = new Task();
			task.setTaskFile(file);
			task.setRequester(user);
			task.setTask(b);
			if(file_path.exists()){
				ObjectInputStream oin = new ObjectInputStream(new FileInputStream(file_path));
				Object obj = (Object)oin.readObject();
				ArrayList<Task> tasklist = (ArrayList<Task>)obj;
				tasklist.add(task);
				oin.close();
				ObjectOutputStream oout = new ObjectOutputStream(new FileOutputStream(file_path));
				oout.writeObject(tasklist);
				oout.flush();
				oout.close();
			} else {
				ArrayList<Task> tasklist = new ArrayList<Task>();
				tasklist.add(task);
				ObjectOutputStream oout = new ObjectOutputStream(new FileOutputStream(file_path));
				oout.writeObject(tasklist);
				oout.flush();
				oout.close();
			}
			RequestDispatcher rd=request.getRequestDispatcher("Publish.jsp?t1=Task uploaded successfully");
			rd.forward(request, response);
		} else{
			RequestDispatcher rd=request.getRequestDispatcher("Publish.jsp?t1=Error in uploading task");
			rd.forward(request, response);
		}
	}catch(Exception e){
		e.printStackTrace();
	}
}

}
