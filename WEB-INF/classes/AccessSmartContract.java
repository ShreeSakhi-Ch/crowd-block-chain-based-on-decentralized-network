package com;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.Contract;
import org.web3j.tx.ManagedTransaction;
import org.web3j.tx.Transfer;
import org.web3j.utils.Convert;
import org.web3j.protocol.core.RemoteCall;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.io.BufferedReader;
import java.io.FileReader;
import java.math.BigInteger;
import java.util.ArrayList;
public class AccessSmartContract {
    public static Web3j web3j;
	public static Credentials credentials;
	public static String address;
public static void setup() {
	try{
		if(web3j == null){
			web3j = Web3j.build(new HttpService());
			credentials = WalletUtils.loadCredentials("blockchain","C:/ETH/data-private/keystore/UTC--2020-01-22T04-33-13.998428700Z--b6ecc4bfac92a1d5fc1aad218bcb7dd6814c90a9");
            TransactionReceipt transferReceipt = Transfer.sendFunds(web3j, credentials,"0xb6ecc4bfac92a1d5fc1aad218bcb7dd6814c90a9", BigDecimal.valueOf(100), Convert.Unit.ETHER).sendAsync().get();
			BufferedReader br = new BufferedReader(new FileReader(Path.getPath()));
			address = br.readLine();
			address = address.trim();
			br.close();
			System.out.println("Transaction complete : "+ transferReceipt.getTransactionHash()+" "+address);
		}
	}catch(Exception e){
		e.printStackTrace();
	}
}



public static String userRegister(String record){
	String result = "none";
	try{
		setup();
        CrowdBCContract pd = CrowdBCContract.load(address, web3j, credentials, ManagedTransaction.GAS_PRICE, Contract.GAS_LIMIT);
		String data = pd.getUser().send();
		if(data.length() > 0){
			data = data+"#"+record;
		}else{
			data = record;
		}
		pd.userRegister(data).send();
		result = "success";
	}catch(Exception e){
		e.printStackTrace();
	}
	return result;
}
public static ArrayList<String> getUser(){
	ArrayList<String> result = new ArrayList<String>();
	try{
		setup();
        CrowdBCContract pd = CrowdBCContract.load(address, web3j, credentials, ManagedTransaction.GAS_PRICE, Contract.GAS_LIMIT);
		String data = pd.getUser().send();
		String arr[] = data.split("#");
		for(int i=0;i<arr.length;i++){
			String record = arr[i];
			if(!record.equals("temp")){
				result.add(record);
			}
		}
	}catch(Exception e){
		e.printStackTrace();
	}
	return result;
}


public static String publishTask(String record){
	String result = "none";
	try{
		setup();
        CrowdBCContract pd = CrowdBCContract.load(address, web3j, credentials, ManagedTransaction.GAS_PRICE, Contract.GAS_LIMIT);
		String data = pd.getPublishTask().send();
		if(data.length() > 0){
			data = data+"#"+record;
		}else{
			data = record;
		}
		pd.publishTask(data).send();
		result = "success";
	}catch(Exception e){
		e.printStackTrace();
	}
	return result;
}
public static ArrayList<String> getPublishTask(){
	ArrayList<String> result = new ArrayList<String>();
	try{
		setup();
        CrowdBCContract pd = CrowdBCContract.load(address, web3j, credentials, ManagedTransaction.GAS_PRICE, Contract.GAS_LIMIT);
		String data = pd.getPublishTask().send();
		String arr[] = data.split("#");
		for(int i=0;i<arr.length;i++){
			String record = arr[i];
			result.add(record);
		}
	}catch(Exception e){
		e.printStackTrace();
	}
	return result;
}

public static String WRContract(String record){
	String result = "none";
	try{
		setup();
        CrowdBCContract pd = CrowdBCContract.load(address, web3j, credentials, ManagedTransaction.GAS_PRICE, Contract.GAS_LIMIT);
		String data = pd.getWRContract().send();
		if(data.length() > 0){
			data = data+"#"+record;
		}else{
			data = record;
		}
		pd.WRContract(data).send();
		result = "success";
	}catch(Exception e){
		e.printStackTrace();
	}
	return result;
}
public static ArrayList<String> getWRContract(){
	ArrayList<String> result = new ArrayList<String>();
	try{
		setup();
        CrowdBCContract pd = CrowdBCContract.load(address, web3j, credentials, ManagedTransaction.GAS_PRICE, Contract.GAS_LIMIT);
		String data = pd.getWRContract().send();
		String arr[] = data.split("#");
		for(int i=0;i<arr.length;i++){
			String record = arr[i];
			result.add(record);
		}
	}catch(Exception e){
		e.printStackTrace();
	}
	return result;
}
}