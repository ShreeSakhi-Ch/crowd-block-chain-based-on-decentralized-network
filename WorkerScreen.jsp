<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>Crowdsourcing</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
<div id="wrapper">
	<div id="header">
		<div id="logo">
			<h1>CrowdBC: A Blockchain-based Decentralized
Framework for Crowdsourcing</h1>
		</div>
		<div id="slogan">
			
		</div>
	</div>
	<div id="menu">
		<ul>
			<li class="first current_page_item"><a href="ViewPublish.jsp">View Publish Task</a></li>
			<li><a href="ViewReward.jsp">View Reward/Penalty</a></li>
			<li><a href="Logout.jsp">Logout</a></li>
		</ul>
		<br class="clearfix" />
	</div>
	<div id="splash">
		<img class="pic" src="images/investor.jpg" width="870" height="230" alt="" />
	</div>
	<br/>
	<h2><b><center><%if(request.getParameter("t1") != null)%>
	<%=request.getParameter("t1")%>!
	</center></b></h2>
	<p align="justify"><font size="3" style="font-family: Comic Sans MS">
	Abstract-Crowdsourcing systems which utilize the human intelligence to solve complex tasks have gained considerable interest and
adoption in recent years. However, the majority of existing crowdsourcing systems rely on central servers, which are subject to the
weaknesses of traditional trust-based model, such as single point of failure. They are also vulnerable to distributed denial of service
(DDoS) and Sybil attacks due to malicious users involvement. In addition, high service fees from the crowdsourcing platform may
hinder the development of crowdsourcing.
</p>
</body>
</html>
