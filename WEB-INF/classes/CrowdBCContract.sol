pragma solidity >=0.4.22 <0.6.0;
contract CrowdBCContract {

  string user_register;
  string publish_task;
  string worker_register_contract;

  function userRegister(string memory ur) public  {
      user_register = ur;
  }
  function getUser() public view returns (string memory) {
      return user_register;
   }
   function publishTask(string memory pt) public  {
      publish_task = pt;
  }
  function getPublishTask() public view returns (string memory) {
      return publish_task;
   }
   function WRContract(string memory wrc) public  {
      worker_register_contract = wrc;
  }
  function getWRContract() public view returns (string memory) {
      return worker_register_contract;
   }
}